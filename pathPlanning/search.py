
# Grid format:
#   0 = Navigable space
#   1 = Occupied space

grid = [[0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 1, 0],
        [0, 0, 1, 1, 1, 0],
        [0, 0, 1, 0, 1, 0]]
init = [0, 0]
goal = [len(grid) - 1, len(grid[0]) - 1]
cost = 1

delta = [[-1, 0],  # go up
         [0, -1],  # go left
         [1, 0],  # go down
         [0, 1]]  # go right

delta_name = ['^', '<', 'v', '>']


def search(grid, init, goal, cost):
    delta = [[-1, 0],  # go up
             [0, -1],  # go left
             [1, 0],  # go down
             [0, 1]]  # go right
    visited = []
    feasible = []
    open = []

    current = [0, init[0], init[1]]
    visited.append([current[1], current[2]])
    # index_up    =  [current[1]-1, current[2]+0]
    # index_left  =  [current[1]+0, current[2]-1]
    # index_down  =  [current[1]+1, current[2]+0]
    # index_right =  [current[1]+0, current[2]+1]

    while current[1] != goal[0] or current[2] != goal[1]:
        # print('goal {}'.format([goal]))
        index_up = [current[1] - 1, current[2] + 0]
        index_left = [current[1] + 0, current[2] - 1]
        index_down = [current[1] + 1, current[2] + 0]
        index_right = [current[1] + 0, current[2] + 1]

        if index_up[0] >= 0 and index_up[0] <= len(grid) - 1 and index_up[1] >= 0 and index_up[1] <= len(
                grid[1]) - 1 and grid[index_up[0]][index_up[1]] != 1 and [index_up[0], index_up[1]] not in visited:
            feasible_up = [current[0] + cost, index_up[0], index_up[1]]
            # print('adding up to open')
            open.append(feasible_up)

        if index_left[0] >= 0 and index_left[0] <= len(grid) - 1 and index_left[1] >= 0 and index_left[1] <= len(
                grid[1]) - 1 and grid[index_left[0]][index_left[1]] != 1 and [index_left[0],
                                                                              index_left[1]] not in visited:
            feasible_left = [current[0] + cost, index_left[0], index_left[1]]
            # print('adding left to open')
            open.append(feasible_left)

        if index_down[0] >= 0 and index_down[0] <= len(grid) - 1 and index_down[1] >= 0 and index_down[1] <= len(
                grid[1]) - 1 and grid[index_down[0]][index_down[1]] != 1 and [index_down[0],
                                                                              index_down[1]] not in visited:
            feasible_down = [current[0] + cost, index_down[0], index_down[1]]
            # print('adding down to open')
            open.append(feasible_down)

        if index_right[0] >= 0 and index_right[0] <= len(grid) - 1 and index_right[1] >= 0 and index_right[1] <= len(
                grid[1]) - 1 and grid[index_right[0]][index_right[1]] != 1 and [index_right[0],
                                                                                index_right[1]] not in visited:
            feasible_right = [current[0] + cost, index_right[0], index_right[1]]
            # print('adding right{} to open'.format(feasible_right))
            open.append(feasible_right)

        costs_open = []
        # print('open : {}'.format(open))

        for i in range(len(open)):
            costs_open.append(open[i][0])

        # print('costs : {}'.format(costs_open))
        if len(costs_open) == 0:
            path = 'fail'
            break
        else:
            minpos = costs_open.index(min(costs_open))
        current = open[minpos]
        visited.append([current[1], current[2]])
        # print('removing {} from open'.format(open[minpos]))
        del open[minpos]
        # print('open updated: {}'.format(open))
        # print('current pos is : {}'.format([current[1],current[2]]))
        # print('visited : {}'.format(visited))
        path = current
    return path


path = search(grid, init, goal, cost)
print(path)

